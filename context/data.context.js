import { createContext, useEffect, useReducer } from 'react';

export const DataContext = createContext({
  state: {
    selectedLocation: '',
    directionMode: false,
    origin: '',
    destination: '',
    routeLine: '',
    userPosition: '',
    routes: [],
    steps: [],
    loading: false,
    focus: 'origin',
    selectedRoute: '',
    mapCenter: '',
  },
  dispatch: () => {},
});

const DataContextProvider = ({ children }) => {
  const reducer = (state, payload) => {
    switch (payload.type) {
      case 'setSelectedLocation':
        return { ...state, selectedLocation: payload.data };
      case 'setDirectionMode':
        return { ...state, directionMode: payload.data };
      case 'setOrigin':
        return { ...state, origin: payload.data };
      case 'setDestination':
        return { ...state, destination: payload.data };
      case 'setRouteInfo': {
        if (state.focus === 'origin')
          return {
            ...state,
            origin: payload.data,
            focus: 'destination',
           
          };
        return { ...state, destination: payload.data };
      }
      case 'setRouteLine':
        return { ...state, routeLine: payload.data };
      case 'setUserPosition':
        return { ...state, position: payload.data };
      case 'setSteps':
        return { ...state, steps: payload.data };
      case 'setRoutes':
        return { ...state, routes: payload.data };
      case 'setSlectedRoute':
        return { ...state, selectedRoute: payload.data };
      case 'setLoading':
        return { ...state, loading: payload.data };
      case 'setFocus':
        return { ...state, focus: payload.data };
      case 'setMapCenter':
        return { ...state, mapCenter: payload.data };
      case 'setState':
        return { ...state, ...payload.data };

      default:
        return { ...payload.data };
    }
  };

  const initialState = {
    selectedLocation: '',
    directionMode: false,
    origin: '',
    destination: '',
    routeLine: '',
    userPosition: '',
    routes: [],
    steps: [],
    loading: false,
    focus: 'origin',
    mapCenter: '',
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  const handleUserPosition = (position) => {
    dispatch('setUserPosition', position);
  };

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(handleUserPosition);
  }, []);

  return (
    <DataContext.Provider value={{ state, dispatch }}>
      {children}
    </DataContext.Provider>
  );
};

export default DataContextProvider;
