import { useRouter } from 'next/router';
import { useContext } from 'react';
import Rating from '@material-ui/lab/Rating';
import { Button, Divider } from '@material-ui/core';
import RoomOutlinedIcon from '@material-ui/icons/RoomOutlined';
import BtnBox from './BtnBox';
import { DataContext } from '../context/data.context';

const Result = () => {
  const router = useRouter();
  const DC = useContext(DataContext);
  const { dispatch } = DC;
  const data = DC.state.selectedLocation;

  const handleClose = () => {
    router.push('/');
    dispatch({ type: 'setSelectedLocation', data: '' });
  };
  return (
    <div style={{ padding: '', position: 'absolute', top: 0 }}>
      <div>
        <img
          src="https://www.ajactraining.org/wp-content/uploads/2019/09/image-placeholder.jpg"
          alt="place"
          style={{ width: '100%', height: '100%', objectFit: 'cover' }}
        />
      </div>
      <div style={{ padding: '10px' }}>
        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <h2>
            <strong>{data.title}</strong>
          </h2>
          <span>{data.region}</span>
          <div
            style={{
              fontSize: '0.8rem',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <span>3/5</span>
            <Rating value={3} size="small" />
            <span>(8)</span>
          </div>
        </div>

        <Divider style={{ marginTop: '20px' }} />
        <BtnBox />
        <Divider style={{ marginBottom: '20px' }} />
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            marginBottom: '20px',
          }}
        >
          <RoomOutlinedIcon style={{ color: '#1A73E8 ', marginLeft: '10px' }} />
          <span>{data.address}</span>
        </div>
        <div
          style={{ width: '100%', display: 'flex', justifyContent: 'center' }}
        >
          <Button
            onClick={handleClose}
            color="primary"
            variant="outlined"
            style={{ borderRadius: '50px' }}
          >
            بازگشت
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Result;
