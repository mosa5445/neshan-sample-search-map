import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import style from '../style/Steps.module.css';

const Steps = ({ steps }) => {
  return (
    <div className={style.root}>
      {steps.map((step) => (
        <div className={style.routeBox} key={step.instruction + step.distance.text}>
          <SubdirectoryArrowRightIcon style={{ color: '#C5CAE9' }} />
          <div className={style.info}>
            <strong>{step.instruction}</strong>
            <span>{step.distance.text}</span>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Steps;
