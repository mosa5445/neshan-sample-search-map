/* eslint-disable no-nested-ternary */
import { useState, useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import useAxios from '../hooks/useAxios';
import SearchField from './SearchField';
import SearchLoding from './SearchLoding';
import ResultBox from './ResultBox';
import Result from './Result';
import { DataContext } from '../context/data.context';
import Direction from './Direction';
import DirectionResult from './DirectionResult';
import PrivatePlace from './PrivateLocation';
import Steps from './Steps';
import Route from './Route';
import RouteInfo from './RouteInfo';
import style from '../style/search.module.css';

const Search = () => {
  // context
  const DC = useContext(DataContext);
  const { state, dispatch } = DC;

  // axios
  const api = useAxios();

  // state
  const [q, setQ] = useState('');
  const [result, setResult] = useState([]);
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  // search function
  const search = async () => {
    try {
      setLoading(true);
      const res = await api.GET(
        `${process.env.NEXT_PUBLIC_NESHAN_API}/v1/search?term`,
        {
          term: q,
          lat:
            DC.state?.userPosition?.coords?.latitude ||
            state?.mapCenter?.lat ||
            0,
          lng:
            DC.state?.userPosition?.coords?.longitude ||
            state?.mapCenter?.lng ||
            0,
        }
      );
      setResult(res.data.items);
      setLoading(false);
    } catch (err) {
      alert('خطایی رخ داده');
      console.log(err);
    }
  };

  // watch for user input
  useEffect(() => {
    if (q.length > 2) search();
    else setResult([]);
  }, [q]);

  return (
    <div className={style.searchBox}>
      {DC.state.directionMode ? (
        <>
          {DC.state?.selectedRoute?.summary ? (
            <RouteInfo />
          ) : (
            <>
              <Direction />
              <DirectionResult />
            </>
          )}

          {DC.state?.routes?.length && !DC.state?.steps?.length ? (
            <Route routes={DC.state.routes} />
          ) : (
            ''
          )}
          {DC.state?.steps?.length ? <Steps steps={DC.state.steps} /> : ''}
        </>
      ) : (
        <SearchField
          q={q}
          setQ={setQ}
          setDirectionMode={(data) =>
            dispatch({ type: 'setDirectionMode', data })
          }
        />
      )}
      {loading ? (
        <SearchLoding />
      ) : router.query.location ? (
        // show result component if location selected
        <Result />
      ) : !DC.state.directionMode ? (
        // show all results
        <>
          <ResultBox items={result} />
          {!result.length ? <PrivatePlace /> : ''}
        </>
      ) : (
        ''
      )}
    </div>
  );
};

export default Search;
