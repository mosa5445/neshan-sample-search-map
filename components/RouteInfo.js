import { useContext } from 'react';

import DriveEtaIcon from '@material-ui/icons/DriveEta';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { IconButton } from '@material-ui/core';
import style from '../style/RouteInfo.module.css';
import { DataContext } from '../context/data.context';

const RouteInfo = () => {
  const DC = useContext(DataContext);
  const { state, dispatch } = DC;

  return (
    <div className={style.root}>
      <div className={style.box}>
        <div className={style.infoBox}>
          <div>
            <DriveEtaIcon />
          </div>
          <div className={style.info}>
            <div style={{ marginBottom: '10px' }}>
              <strong>مسیر </strong>
              <strong>{state.selectedRoute.summary}</strong>
            </div>
            <div>
              <span>مسافت: </span>
              <span>{state.selectedRoute.distance}</span>
            </div>
            <div>
              <span>در مدت: </span>
              <span>{state.selectedRoute.duration}</span>
            </div>
          </div>
        </div>
        <div>
          <IconButton
            onClick={() =>
              dispatch({
                type: 'setState',
                data: {
                  steps: [],
                  selectedRoute: '',
                },
              })
            }
          >
            <ArrowBackIcon style={{ color: '#fff' }} />
          </IconButton>
        </div>
      </div>
    </div>
  );
};

export default RouteInfo;
