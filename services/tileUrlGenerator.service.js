/* eslint-disable no-param-reassign */
/* eslint-disable no-bitwise */
function tileUrlGenerator(x, y, z, Url) {
  const key = String(((x << z) + (y << (z - 1))) * z);
  const value = String(((y << (z + 1)) - (x << z)) * (z - 1));

  Url = Url.replace('{z}', z).replace('{x}', x).replace('{y}', y);
  Url += `?x=${key}&y=${value}`;

  return Url;
}

export default tileUrlGenerator
